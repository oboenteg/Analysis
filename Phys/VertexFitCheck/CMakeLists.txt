################################################################################
# Package: VertexFitCheck
################################################################################
gaudi_subdir(VertexFitCheck v1r10)

gaudi_depends_on_subdirs(Event/LinkerEvent
                         Event/MCEvent
                         Phys/DaVinciAssociators
                         Phys/DaVinciKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VertexFitCheck
                 src/*.cpp
                 LINK_LIBRARIES LinkerEvent MCEvent DaVinciKernelLib)

