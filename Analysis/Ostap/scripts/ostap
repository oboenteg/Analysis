#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
# $Id$ 
# =============================================================================
## @file ostap
#  
#     .oooooo.                .                        
#    d8P'  `Y8b             .o8                        
#   888      888  .oooo.o .o888oo  .oooo.   oo.ooooo.  
#   888      888 d88(  "8   888   `P  )88b   888' `88b 
#   888      888 `"Y88b.    888    .oP"888   888   888 
#   `88b    d88' o.  )88b   888 . d8(  888   888   888 
#    `Y8bood8P'  8""888P'   "888" `Y888""8o  888bod8P' 
#                                            888       
#                                           o888o      
#                                                    
#  Simple interactive PyRoot-based analysis environment to provide access
#  to zillions useful decorators for ROOT (and not only ROOT!) objects&classes  
# 
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
#  @date   2012-02-15
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#
#                    $Revision$
#  Last modification $Date$
#                 by $Author$
# =============================================================================
"""
Simple interactive PyRoot-based analysis environment
to provide access to zillions useful decorators for ROOT
(and not only ROOT) objects&classes

This file is a part of BENDER project:

``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign
of Dr.O.Callot et al.:

``No Vanya's lines are allowed in LHCb/Gaudi software''
"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__    = "2012-09-10"
__version__ = '$Revision$'
# =============================================================================
import ROOT 
ROOT.PyConfig.IgnoreCommandLineOptions = True
# =============================================================================

## perform the actual start of ostap 
from Ostap.Run import *

# =============================================================================
if with_ipython () :
    raise RuntimeError("Can't start Ostap from ipython!")


# =============================================================================
## exit if batch 
if arguments.batch :
    
    ## nothing to do 
    logger.debug  ('Bye, bye...')

elif arguments.embed : ## the only one option up to Ostap v1r9

    logger.info ('Start embedded interactive shell')
    
    import IPython
    IPython.embed ()
    
elif arguments.simple : ## new from Ostap v1r10 
        
    __vars = globals().copy()
    __vars.update( locals() )
    
    import readline
    import code
    
    logger.info ('Start simple interactive shell') 
    
    shell = code.InteractiveConsole(__vars)
    shell.interact()
    
else :                   ## new from Ostap v1r10 

    __vars = globals().copy()
    __vars.update( locals() )
    
    logger.info ('Start interactive shell') 
    
    import IPython
    IPython.start_ipython ( argv = [] , user_ns = __vars )


# =============================================================================
# The END 
# =============================================================================

